import Vue from 'vue'
import Vuelidate from 'vuelidate'

import App from './App.vue'
import './registerServiceWorker'
import router from './router'
import store from './store'

import 'materialize-css/dist/js/materialize.min'

import tooltipDirective from '@/directives/tooltip.directive'
import messagePlugin from './utils/message.plugin'

import Paginate from 'vuejs-paginate'
import Loader from './components/app/Loader'

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

import dateFilter from './filters/date.filter'
import currencyFilter from './filters/currency.filter'
import localizeFilter from './filters/localize.filter'

Vue.config.productionTip = false

Vue.use(messagePlugin)
Vue.use(Vuelidate)
Vue.filter('date', dateFilter)
Vue.filter('currency', currencyFilter)
Vue.filter('localize', localizeFilter)
Vue.directive('tooltip', tooltipDirective)
Vue.component('Paginate', Paginate)
Vue.component('Loader', Loader)

firebase.initializeApp({
  apiKey: 'AIzaSyBQKuNLOmz3RbYKIoky43bV5j3I-D9AuVQ',
  authDomain: 'vue-crm-ab257.firebaseapp.com',
  databaseURL: 'https://vue-crm-ab257.firebaseio.com',
  projectId: 'vue-crm-ab257',
  storageBucket: 'vue-crm-ab257.appspot.com',
  messagingSenderId: '458136768101',
  appId: '1:458136768101:web:e91d7d47b7e704a437808c',
  measurementId: 'G-8MMB8WYFBP',
})

let app

firebase.auth().onAuthStateChanged(() => {
  if (!app) {
    app = new Vue({
      router,
      store,
      render: h => h(App),
    }).$mount('#app')
  }
})
